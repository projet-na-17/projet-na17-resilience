# Résilience

## Objets de la BDD et Propriétés
* Entité
    * Nom : chaîne de caractères
* Personne
    * Prénom : chaîne de caractères
* Coordonnées
    * x : double
    * y : double
* Communauté
* Lien
    * Source : Personne
    * Cible : Personne
    * Description : chaîne de caractères
* Savoir-faire
    * Nom : chaîne de caractère
* Service
    * Nom : chaîne de caractère
    * Description : chaîne de caractère
    * Contrepartie : entier non signé
* Service-contrepartie
	* Nom : chaîne de caractère
	* Description : chaîne de caractère
* Compte
    * Clé publique : chaîne de caractère
* Message
    * Expéditeur : Entité
    * Destinataire : Entité
    * Contenu : chaîne de caractère
    * Référence : Message
* Vote d'opposition
    * Cible : Personne
    * Communauté : Communauté
    * Votant : Personne
* Détenir
    * Entité : Entité
    * Savoir-faire : Savoir-faire
    * Degré : entier


## Contraintes
* Entité
    * Le nom ne doit pas être nul
* Personne
    * Hérite de Entité
    * Le prénom ne doit pas être nul
* Coordonnées
    * Les coordonnées x et y ne doivent pas être nul
* Communauté
    * Hérite de Entité
* Lien
    * Unidirectionnel
    * La description ne doit pas être nul
* Savoir-faire
    * Le nom ne doit pas être nul
* Service
    * Le nom ne doit pas être nul
    * La description ne doit pas être nul
    * Si la contrepartie
        * est égal à zéro : on attend aucune contrepartie
        * est non nulle : il s'agit du coût en G1
        * n'est pas renseignée : si il y a un lien avec un service-contrepartie, il s'agit de la contrepartie
        * sinon, la contrepartie est à discuter
* Service-contrepartie
	* Le nom ne doit pas être nul
	* La description ne doit pas être nul
* Compte
    * La clé publique ne doit pas être nul
* Message
    * L'expéditeur, le destinataire et le contenu ne doivent pas être nuls
    * L'expéditeur ne peut pas être le même que le destinataire
* Vote d'opposition
    * La cible, la communauté et le votant ne doivent pas être nuls
    * La cible ne peut pas être la même que le destinataire
    * La cible et le votant doivent appartenir à la communauté
* Détenir
    * Le degré doit être compris entre 1 et 5
    * L'entité doit être une Entité
    * Le savoir-faire doit être un Savoir-Faire


## Rôles
* Administrateur
    * Possède tous les droits
* Utilisateur
    * A le droit d'insérer, de modifier et de sélectionner dans la table Entité
    * A le droit d'insérer, de modifier et de sélectionner dans la table Personne
    * A le droit d'insérer, de modifier et de sélectionner dans la table Communauté
    * A le droit d'insérer et de sélectionner dans la table Coordonnées
    * A le droit d'insérer, de modifier, de sélectionner et de supprimer dans la table Service
    * A le droit d'insérer, de modifier, de sélectionner et de supprimer dans la table Service-contrepartie
    * A le droit d'insérer, de modifier, de sélectionner et de supprimer dans la table Lien
    * A le droit d'insérer, de modifier et de sélectionner dans la table Savoir-faire
    * A le droit d'insérer, de modifier, de sélectionner et de supprimer dans la table Compte
    * A le droit d'insérer, de modifier, de sélectionner et de supprimer dans la table Message
    * A le droit d'insérer, de modifier, de sélectionner et de supprimer dans la table Vote d'opposition
    * A le droit d'insérer, de modifier, de sélectionner et de supprimer dans la atable Détenir



## Fonctions
* Entité
    * échanger un message
    * déclarer possèder un savoir-faire
    * se localiser
    * se lier avec d'autres communautés
    * créer un compte
* Personne
    * créer une communauté
    * s'inscrire dans une communauté
    * se lier avec d'autres personnes
    * s'opposer à la présence d'une autre personne
    * créer un savoir-faire
    * proposer un service
    * administrer les informations de la communauté
* Coordonnées
    * générer un lien openstreetmap
* Vote d'opposition
    * compter le nombre de votes au sein d'une communauté envers une personne
* Détenir
    * vérifier que le degré soit compris entre 1 et 5