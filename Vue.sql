CREATE OR REPLACE FUNCTION calculNbVote (id_commu INT, id_pers INT)
RETURNS BIT AS $vote$
    DECLARE
		nbVotant INT = 0;
        	nbPersonneCommu INT = 0;
	BEGIN
		SELECT COUNT(id_votant) INTO nbVotant FROM VOTEOPPOSITION WHERE id_personne = id_pers AND id_communaute=id_commu;
		SELECT COUNT(p.id) INTO nbPersonneCommu FROM Appartenir a INNER JOIN Communaute c ON c.id=a.id_communaute INNER JOIN Personne p ON p.id=a.id_personne WHERE c.id=id_commu;
		IF nbVotant > (nbPersonneCommu - 1) / 2
			THEN RETURN 1;
		ELSE
			RETURN 0;
		END IF;
	END;
$vote$ LANGUAGE plpgsql;

DROP VIEW IF EXISTS vue_communaute;

CREATE VIEW vue_communaute AS
SELECT P.id,P.prenom,E.nom nom, calculNbVote(C.id,P.id) exclu
FROM ENTITE E, APPARTENIR A, COMMUNAUTE C, PERSONNE P
WHERE E.id = C.id AND C.id=A.id_communaute AND P.id = A.id_personne;


CREATE OR REPLACE FUNCTION seakRacine(id_message INT)
RETURNS INT AS $racine$
	DECLARE
		previousMessage INT;
		racine INT;
	BEGIN
		WHILE RACINE IS NULL
		LOOP
			SELECT m.id_reference INTO previousMessage FROM Message m WHERE id=id_message;
			IF previousMessage IS NULL
				THEN racine = id_message;
			ELSE
				id_message=previousMessage;
			END IF;
		END LOOP;
		RETURN racine;
	END;
$racine$ LANGUAGE plpgsql;

DROP VIEW IF EXISTS vue_message;

CREATE VIEW vue_message AS
SELECT id_expediteur expediteur, id_destinataire destinataire, id_reference reference, contenu, seakRacine(id) racine
FROM Message m; 

CREATE OR REPLACE FUNCTION checkPeople(id_request INT)
RETURNS TABLE(id_entite INT,id_proche INT) AS $table$
DECLARE
	nextPersonne INT;
	x1 INT;
	y1 INT;
	x2 INT;
	y2 INT;
BEGIN
	SELECT c.x, c.y INTO x1,y1
	FROM COORDONNEE c,ENTITE e
	WHERE e.id=e.id AND e.id=id_request;

	SELECT id INTO nextPersonne FROM Entite LIMIT 1;

	WHILE nextPersonne IS NOT NULL
	LOOP
		IF nextPersonne != id_request
			THEN
				SELECT x,y INTO x1,y2
				FROM COORDONNEE c,ENTITE e
				WHERE e.id=c.id AND e.id=nextPersonne;
				IF checkDistance(x1,y1,x2,y2) = 1
					THEN 
						id_entite:=id_request;
						id_proche:=nextPersonne;
						RETURN NEXT;
				END IF;
			END IF;
			SELECT id INTO nextPersonne FROM Entite WHERE id>nextPersonne LIMIT 1;
	END LOOP;
	RETURN;
END;
$table$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkDistance(x1 float, y1 float, x2 float, y2 float)
RETURNS INT AS $proche$
DECLARE sum float;
BEGIN
	sum=ACOS(SIN(RADIANS(x1))*SIN(RADIANS(x2))+COS(RADIANS(x1))*COS(RADIANS(x2))*COS(RADIANS(y1-y2)))*6371;
	IF SUM > 1
		THEN RETURN 0;
	ELSE
		RETURN 1;
	END IF;
END;
$proche$ LANGUAGE plpgsql;

		



