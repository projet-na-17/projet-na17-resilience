CREATE TABLE IF NOT EXISTS ENTITE(
    id SERIAL PRIMARY KEY,
    nom VARCHAR(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS PERSONNE(
    id INTEGER,
    prenom VARCHAR(30) NOT NULL,
    FOREIGN KEY (id) REFERENCES ENTITE(id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS COMMUNAUTE(
    id INTEGER PRIMARY KEY,
    FOREIGN KEY (id) REFERENCES ENTITE(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS LIEN(
    id_source INTEGER,
    id_cible INTEGER,
    description TEXT NOT NULL,
    FOREIGN KEY (id_source) REFERENCES ENTITE(id) ON DELETE CASCADE,
    FOREIGN KEY (id_cible) REFERENCES ENTITE(id) ON DELETE CASCADE,
    PRIMARY KEY(id_cible,id_source)
);

CREATE TABLE IF NOT EXISTS COORDONNEE(
    id INTEGER PRIMARY KEY,
    x FLOAT,
    y FLOAT,
    FOREIGN KEY(id) REFERENCES ENTITE(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS APPARTENIR(
    id_personne INTEGER,
    id_communaute INTEGER,
    PRIMARY KEY(id_personne,id_communaute),
    FOREIGN KEY(id_personne) REFERENCES PERSONNE(id) ON DELETE CASCADE,
    FOREIGN KEY(id_communaute) REFERENCES COMMUNAUTE(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS VOTEOPPOSITION(
    id_personne INTEGER,
    id_votant INTEGER,
    id_communaute INTEGER,
    PRIMARY KEY(id_personne,id_votant,id_communaute),
    FOREIGN KEY(id_personne) REFERENCES PERSONNE(id) ON DELETE CASCADE,
    FOREIGN KEY(id_votant) REFERENCES PERSONNE(id) ON DELETE CASCADE,
    FOREIGN KEY(id_communaute) REFERENCES COMMUNAUTE(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS SERVICE(
    id SERIAL,
    id_entite INTEGER NOT NULL,
    nom VARCHAR(30) NOT NULL,
    description TEXT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(id_entite) REFERENCES ENTITE(id) ON DELETE CASCADE
   );

CREATE TABLE IF NOT EXISTS COMPTE(
    id SERIAL,
    id_entite INTEGER,
    cle_publique TEXT NOT NULL,
    FOREIGN KEY(id_entite) REFERENCES ENTITE(id) ON DELETE SET NULL,
    PRIMARY KEY(id)
);
CREATE TABLE IF NOT EXISTS SAVOIRFAIRE(
   id SERIAL PRIMARY KEY,
   nom VARCHAR(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS SAVOIRFAIRE_SERVICE(
    id_service INTEGER,
    id_savoirfaire INTEGER, 
    FOREIGN KEY(id_service) REFERENCES SERVICE(id) ON DELETE CASCADE,
    FOREIGN KEY(id_savoirfaire) REFERENCES SAVOIRFAIRE(id) ON DELETE CASCADE,
    PRIMARY KEY(id_service, id_savoirfaire)
);

CREATE TABLE IF NOT EXISTS SERVICE_CONTREPARTIE(
    id SERIAL PRIMARY KEY,
    nom VARCHAR(30),
    description TEXT,
    id_service INTEGER NOT NULL,
    FOREIGN KEY(id_service) REFERENCES SERVICE(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS DETENIR(
    degre INTEGER NOT NULL,
    id_entite INTEGER,
    id_savoir_faire INTEGER,
    FOREIGN KEY(id_entite) REFERENCES ENTITE(id) ON DELETE CASCADE,
    FOREIGN KEY(id_savoir_faire) REFERENCES SAVOIRFAIRE(id) ON DELETE CASCADE,
    PRIMARY KEY(id_entite,id_savoir_faire),
    CHECK (degre > 0 AND degre <= 5)
);

CREATE TABLE IF NOT EXISTS MESSAGE(
    id SERIAL,
    id_destinataire INTEGER NOT NULL,
    id_expediteur INTEGER NOT NULL,
    id_reference INTEGER,
    contenu TEXT NOT NULL,
    FOREIGN KEY(id_destinataire) REFERENCES ENTITE(id) ON DELETE CASCADE,
    FOREIGN KEY(id_expediteur) REFERENCES ENTITE(id) ON DELETE CASCADE,
    FOREIGN KEY(id_reference) REFERENCES MESSAGE(id) ON DELETE SET NULL,
    PRIMARY KEY(id)
);

INSERT INTO ENTITE (id, nom) VALUES
  ('1', 'Crozat'),
  ('2', 'Dummy'),
  ('3', 'UTC'),
  ('4', 'XIV'),
  ('5', 'Turtle'),
  ('6', 'Animaux'),
  ('7', 'Churchill'),
  ('8', 'Wayne'), 
  ('9', 'Superheros'),
  ('10', 'Kent'),
  ('11', 'Pan'),
  ('12', 'Parker'),
  ('13', 'Doo'),
  ('14', 'Rockstars');

INSERT INTO PERSONNE (id, prenom) VALUES
  ('1', 'Stephane'),
  ('2', 'Toto'),
  ('4', 'Louis'),
  ('5', 'Franklin'),
  ('7', 'Winston'),
  ('8', 'Bruce'),
  ('10', 'Clark'),
  ('11', 'Pan'),
  ('12', 'Peter'),
  ('13', 'Scooby');

INSERT INTO COMMUNAUTE (id) VALUES
  ('3'),
  ('6'),
  ('9');

INSERT INTO APPARTENIR (id_personne, id_communaute) VALUES
  ('1', '3'),
  ('2', '3'),
  ('5', '6'),
  ('8', '9'),
  ('10', '9'),
  ('11', '6'),
  ('12', '9'),
  ('12', '6'),
  ('13', '6');

INSERT INTO COORDONNEE (id, x, y) VALUES
  ('1','49.415266', '2.819131'),
  ('2','49.412981', '2.816566'),
  ('4','48.805202', '2.120662'),
  ('10', '58.220156', '-42.601785'),
  ('12', '45.157895', '3.154789'),
  ('13', '48.804321', '2.915654');

INSERT INTO LIEN (id_source, id_cible ,description) VALUES
  ('1', '2', 'professeur'),
  ('2', '1', 'eleve'),
  ('8', '10', 'rival'),
  ('10', '8', 'rival'),
  ('4', '7', 'correspondant'),
  ('7', '4', 'correspondant'),
  ('13', '5', 'ami');

INSERT INTO VOTEOPPOSITION (id_votant, id_communaute, id_personne) VALUES
  ('1', '3', '2'),
  ('5', '6', '12'),
  ('11', '6', '12'),
  ('13', '6', '12'),
  ('8', '9', '10');

INSERT INTO COMPTE (id, id_entite,cle_publique) VALUES
  ('1', '7', '1FGAftzSTztFSB8LMwsrdCKTyqGY6zr3sU'),
  ('4', '8', '5FkVf89STztB8LDsr4dC6pKTyqG4668ddU'),
  ('7', '10', 'dS54etzSTztFtr8Le3dsdCKTyqpY6dS97r'),
  ('13', '4', '1dQQQtzSdSer8B8LMwsrdCKTyqgfH563sp');

INSERT INTO SAVOIRFAIRE(id,nom) VALUES
  ('1', 'Base de donnees'),
  ('2', 'Faire des lacets'),
  ('3', 'Dire "Scoobydoo"'),
  ('4', 'Sauver le monde'),
  ('5', 'Faire de la politique'),
  ('6', 'Faire la cuisine');

INSERT INTO DETENIR(id_entite, id_savoir_faire, degre) VALUES
  ('1', '1', '5'),
  ('5', '2', '4'),
  ('7', '5', '5'),
  ('12', '4', '4'),
  ('8', '4', '5'),
  ('10', '4', '5'),
  ('13', '3', '5'),
  ('7', '6', '1'),
  ('13', '6', '2'),
  ('10', '6', '4');

INSERT INTO SERVICE(id, id_entite, nom, description) VALUES
  ('1', '1', 'Creation BDD', 'Je possede de fortes connaissances en base de donnees'),
  ('2', '5', 'Faisage de lacets', 'Je sais lacer mes chaussures, mais aussi celle des autres'),
  ('3', '4', 'Gain de puissance', 'Je peux faire de vous un homme  influant à la cour');

INSERT INTO SAVOIRFAIRE_SERVICE (id_service, id_savoirfaire) VALUES
  ('1', '1'),
  ('3', '2');

INSERT INTO SERVICE_CONTREPARTIE (id_service, nom, description) VALUES
  ('1', 'Repassage', 'Mes chemises sont toutes froissées'),
  ('2', 'Divertissement', 'L''ennui me guette. Je souhaite être diverti');

INSERT INTO MESSAGE (id, id_destinataire, id_expediteur, id_reference, contenu) VALUES
  ('1', '6', '9', NULL, 'Hey, les super-héros, vous êtes vraiment des super-vilains !'),
  ('2', '12', '6', '1', 'Calmez-vous les enfants'),
  ('3', '8', '12', '2', 'C''est vraiment des super crétins'),
  ('4', '13', '7', NULL, 'Scooobydoobydoooooooooo !!'),
  ('5', '13', '14', NULL, 'Ooooh yeah !');